#!/bin/bash
qemu-system-x86_64 -smp 2 -m 2048 \
  -drive file=debian.qcow2,if=virtio \
  -netdev user,id=n1,\
hostfwd=tcp::6379-:6379,\
hostfwd=tcp::9000-:9000,\
hostfwd=tcp::2022-:22,\
hostfwd=tcp::4443-:443,\
hostfwd=tcp::44395-:44395, \
  -device virtio-net,netdev=n1 \
  # -cdrom debian-11.6.0-amd64-netinst.iso -boot d \
  # -nographic
