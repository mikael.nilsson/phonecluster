#!/bin/bash

apt update

# # get sudo
# apt -y install sudo
# usermod -aG sudo $USER

# su $USER

# qemu guest agent
sudo apt -y install qemu-guest-agent
sudo systemctl enable qemu-guest-agent
sudo systemctl start qemu-guest-agent

# docker
sudo apt -y install docker.io

