# UPDATE [20230913]

I read this on [stackoverflow](https://stackoverflow.com/questions/53527277/is-it-possible-to-run-containers-on-android-devices)
and am starting to realize the futility of this project. So I'm retiring this for now, maybe trying out [limbo](https://github.com/limboemu/limbo) and will focus on doing similar things with rust or nodejs. Perhaps I'll return to this some time in the future, but probably not.

# An android phone docker cluster

The idea is to run docker and kubernetes on a few old phones.
We need to

- create a debian image containing
  - docker
  - a kubernetes node
- create the cluster
- create a script that in termux on a new phone
  - installs qemu
  - downloads image
  - starts image which on first boot
    - changes hostname
    - joins the kluster

Do we need static ip? ATM I guess not
We do need unique hostnames and a net config that lets the phones communicate

# TODO

- [] we need a smaller image
